import 'package:education/app/app.dart';
import 'package:education/app/bootstrap.dart';

void main() {
  bootstrap(() => const App());
}
